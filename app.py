import flask

app = flask.Flask(__name__)

@app.route('/', methods=['GET'])
def home():
    return "<h1>SNA Lab 13</h1>"

if __name__ == '__main__':
    app.run(debug=True, host="0.0.0.0", port=80)